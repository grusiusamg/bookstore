## About project:

This is simple bookstore e-shop generated with Spring. 

## How to install?

First you will need to create MySQL database with name "bookstoredatabase". Then you will need git clone this project.

Shop could work without admin panel but if you want to add books you can download it here:<br>
https://gitlab.com/grusiusamg/bsadminpanel.git,

All needed commponents for Spring and dependencies from POM.XML install with your IDE and start server.


Shop will open with this url: http://localhost:8081/<br>
Admin panel will open with this url: http://localhost:8082/

E-shop server will automaticly create demo account (name: **grusius**, password: **123456**).<br>
Admin panel will automaticly create admin account(name: **admin**, password: **admin**).

Registration of E-shop going via e-mail. System will automaticaly send an e-mail with the link to your account and your temporary password.<br> After an update of your account you can fully use the store.
