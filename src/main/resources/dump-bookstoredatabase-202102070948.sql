-- MySQL dump 10.13  Distrib 5.5.62, for Win64 (AMD64)
--
-- Host: localhost    Database: bookstoredatabase
-- ------------------------------------------------------
-- Server version	5.5.5-10.4.13-MariaDB

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `billing_address`
--

DROP TABLE IF EXISTS `billing_address`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `billing_address` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `billing_address_city` varchar(255) DEFAULT NULL,
  `billing_address_country` varchar(255) DEFAULT NULL,
  `billing_address_name` varchar(255) DEFAULT NULL,
  `billing_address_street1` varchar(255) DEFAULT NULL,
  `billing_address_street2` varchar(255) DEFAULT NULL,
  `billing_address_zipcode` varchar(255) DEFAULT NULL,
  `order_id` bigint(20) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `FKjg6ji2vsfuqlc9vhvy4yi449h` (`order_id`),
  CONSTRAINT `FKjg6ji2vsfuqlc9vhvy4yi449h` FOREIGN KEY (`order_id`) REFERENCES `user_order` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8mb4;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `billing_address`
--

LOCK TABLES `billing_address` WRITE;
/*!40000 ALTER TABLE `billing_address` DISABLE KEYS */;
INSERT INTO `billing_address` VALUES (1,'mainas','Lithuania','arti','leliju 11','','54789',1),(2,'frankfurt','Lithuania','tadijus','toli 11','','58745',2),(3,'mainas','Lithuania','arti','leliju 11','','54789',3);
/*!40000 ALTER TABLE `billing_address` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `book`
--

DROP TABLE IF EXISTS `book`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `book` (
  `id` bigint(20) NOT NULL,
  `active` bit(1) NOT NULL,
  `author` varchar(255) DEFAULT NULL,
  `category` varchar(255) DEFAULT NULL,
  `description` text DEFAULT NULL,
  `format` varchar(255) DEFAULT NULL,
  `in_stock_number` int(11) NOT NULL,
  `isbn` varchar(255) DEFAULT NULL,
  `language` varchar(255) DEFAULT NULL,
  `list_price` double NOT NULL,
  `number_of_pages` int(11) NOT NULL,
  `our_price` double NOT NULL,
  `publication_date` varchar(255) DEFAULT NULL,
  `publisher` varchar(255) DEFAULT NULL,
  `shipping_weight` double NOT NULL,
  `title` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `book`
--

LOCK TABLES `book` WRITE;
/*!40000 ALTER TABLE `book` DISABLE KEYS */;
INSERT INTO `book` VALUES (1,'','Hiro Arikawa','Arts','\r\n\r\nNana išsiruošė į kelią. Kadaise laukinis katinas, dabar jis patogiai tupi ant priekinės sidabrinio furgono sėdynės greta savo šeimininko Satoru ir žvelgia pro langą į besikeičiančius pribloškiamo grožio Japonijos vaizdus. Satoru jam nepaaiškino, kodėl reikėjo leistis į šią kelionę, bet Nanai nelabai ir rūpi: kad ir kiek ilgai, kad ir kaip toli keliautų, jam svarbiausia – kad šį nuotykį patirs kartu su savo žmogumi. Tačiau, įpusėjus kelionei ir aplankius tris senus Satoru draugus, Nana ima nuvokti, koks tikrasis jų kelionės tikslas ir kaip stipriai tai galų gale pakeis jųdviejų gyvenimus.\r\n\r\n„Kad ir kokie kartais nepaveikiami tariamės esą, tikiu, kad mažai kas baigs skaityti šią knygą nenubraukę vienos kitos nuoširdžios ašaros.\"\r\nNPR\r\n\r\n„Keliaujančio katino kronikos\" eina tradicinių japonų pasakojimų keliu, knygoje išaukštinamos tokios esminės vertybės kaip pasiaukojimas ir draugystė. Džiaugsmas skaityti.\"\r\nFinancial Times\r\n\r\nHiro Arikawa (g. 1972) – japonų rašytoja, gyvenanti ir kurianti Tokijuje. Jos knyga „Keliaujančio katino kronikos\" tapo bestseleriu ne tik Japonijoje, bet ir kitose šalyse – pasaulyje jau parduota daugiau nei 1 milijonas egzempliorių. 2018 m., kuriant filmą pagal šią H. Arikawos istoriją, buvo suburtas kone visas Japonijos kino žvaigždynas.\r\n','paperback',99,'9786094793837','lithuanian',15.29,256,7.64,'2021-02-10','Baltos lankos',0.1,'Keliaujančio katino kronikos'),(2,'','David R. Brooks','Programing','HTML, JavaScript, and PHP are Web-based programming languages that can be used to solve computational problems in an online environment.\r\n\r\nThis easy-to-read, informative guide/reference will enable readers to quickly develop a working knowledge of HTML, JavaScript and PHP - a valuable skill for any scientist or engineer. Updating and expanding upon the author\'s previous Springer titles, An Introduction to HTML and JavaScript and An Introduction to PHP, the text emphasizes a hands-on approach to learning and makes extensive use of examples throughout the book. A detailed science, engineering, or mathematics background is not required to understand the material, making the book ideally suitable for self-study or an introductory course in programming.\r\n\r\nTopics and features: describes the creation and use of HTML documents, including tables, forms, lists, frames, and cascading style sheets; presents fundamental concepts of client-side and server-side programming languages and their application to scientific and engineering calculations, using JavaScript and PHP; examines JavaScript and PHP implementation of arrays, built-in and user-defined methods and functions, math capabilities, and input processing with HTML forms; with PHP, extends programming fundamentals to include reading and writing server-based files, command-line interfaces, and an introduction to GD graphics; appendices include lists of ASCII and HTML special characters, and a brief introduction to using a \"pseudocode\" approach to organizing solutions to computing problems; includes a Glossary and an extensive set of programming exercises.\r\n\r\nThis highly useful guidebook supplies all the tools necessary to begin programming in HTML, JavaScript and PHP for scientific and engineering applications. Its clear writing style, with a focus on the importance of learning by example, will appeal to both professionals and undergraduate students in any technical field. ','hardcover',197,'0857294482','english',91.49,199,90.99,'2011-06-14','Springer-Verlag GmbH',0.1,'Guide to HTML, JavaScript and PHP'),(3,'',' Francis Smith, H. D. Richardson ','Management','','paperback',10,'1444646699','english',29.89,186,25.63,'2009-07-06','Read Books',0.2,'The Canary: Its Varieties, Managment, and Breeding'),(4,'','Tayari Jones','Fiction','\r\n\r\n„Tikėjau, kad mūsų santuoka panaši į gražiai nuaustą gobeleną, netvirtą, bet pataisomą. Mes dažnai jį įplėšdavom ir užadydavom – visada šilkiniu siūlu, dailiu, bet trūkinėjančiu.\"\r\n\r\nJaunavedžiai Selestija ir Rojus turėjo viską: įdomias profesijas, namus, žydinčią meilę – ranka pasiekiamas svajones. Tačiau vieną naktį jų šviesi ateitis subyra, kai Rojus neteisingai apkaltinamas išprievartavimu. Selestija neabejoja savo vyro nekaltumu, ir jiedu pasiryžę ištverti dvylika metų kalėjimo: vienas kitam rašo ilgus meilės laiškus, kantriai laukia dienos, kai vėl susitiks laisvėj. Tačiau net būdama nepriklausoma ir savarankiška Selestija ilgainiui ima ilgėtis peties, ant kurio pasiguostų ir nusiramintų. Ir Rojus tai ima nujausti.\r\n\r\nŠis daugiaspalvis meilės, santuokos, ištikimybės paveikslas – žvilgsnis į širdis ir protus žmonių, kuriuos išskyrė ir negrįžtamai pakeitė nuo jų nepriklausančios aplinkybės. Ar meilė gali ištverti ilgus metus atskirai? Ar santuoka įmanoma negyvenant kartu? Ir ar šių laikų Penelopė liks ištikima ir sulauks grįžtant savo Odisėjo?\r\n\r\n„Viena iš daugybės įgimtų Tayari dovanų – savo žodžiais palytėti mūsų sielas.\"\r\nOprah Winfrey\r\n\r\n„Jaudinantis portretas jaunos afroamerikiečių poros, kurios gyvenimą nulemia neteisinga kalėjimo bausmė.\"\r\nBarack Obama\r\n\r\n„Patiki kiekvienu žodžiu.\"\r\nknyguziurkes.wordpress.com\r\n\r\nTayari Jones (Tajari Džouns, g. 1970) – amerikiečių rašytoja. Parašė keturis romanus. Jos naujausias kūrinys „Nuojautos\" apdovanotas „Women\'s Prize for Fiction\" ir daugeliu kitų literatūros prizų, įtrauktas į Oprah Winfrey knygų klubo skaitinių sąrašą ir Baracko Obamos mėgstamiausių 2018 m. skaitytų kūrinių sąrašą. Romanas išleistas daugiau kaip 20 šalių.\r\n','hardcover',50,'9786094793349','lithuanian',12.59,352,12.36,'2020-01-07','Baltos lankos',0.3,'NUOJAUTOS'),(5,'',' Saulius Grybauskas, Rūta Grišinaitė ','Engineering','Knygoje tyrinėjamas inžinierių sluoksnio formavimasis sovietinėje Lietuvoje 1944–1990 metais. Sovietmečiu inžinieriai sudarė didžiausią aukštąjį išsilavinimą turinčių specialistų grupę. Lietuvių tapsmas inžinieriais pretendavo į tikrą sėkmės istoriją, unikalią visos SSRS kontekste. Lietuvių sodiečių vaikai įgijo ir įvaldė iki tol respublikai mažai žinomas technines profesijas ir ėmė dominuoti tarp inžinierių. Jokioje kitoje sovietinėje respublikoje mes nerasime taip etniškai monolitiško techninės inteligentijos sluoksnio. Nors Maskva iš dalies rėmė vietinių kadrų kėlimą į vadovaujamuosius postus ir respublikų liaudiškas kultūras, kas turėjo pelnyti žmonių simpatijas ir paramą režimui, šiai nacionalinei politikai iššūkį metė sovietinio ūkio plėtros tikslai, industrializacija, kuri grasino pakeisti gyventojų etninę sudėtį, internacionalizuoti kultūrą. Atrodytų, kad inžinieriai, kurie buvo svarbiausi ūkio ir techninės plėtros agentai, ir turėjo tapti tais internacionalizmo nešėjais. Tačiau Lietuvoje, skirtingai nei kaimyninėje Latvijoje ir kitose respublikose, to neįvyko.','paperback',36,'9786098183726','lithuanian',14.39,278,14.39,'2021-02-16','Lietuvos istorijos institutas',0.2,'Inžinieriai pagal planą'),(6,'','Vaiga','Management','Žodynas dviejų - rusų-lietuvių ir lietuvių-rusų kalbų - dalių, daugiau nei 20000 žodžių. Žodyno pagrindą sudaro plačiau vartojami rusų ir lietuvių kalbų žodžiai bei posakiai ir kai kurie populiaresni terminai.','hardcover',699,' 9786094401817','russian',5.32,512,1.23,'2021-02-01','Vaiga',0.6,'Trumpas rusų-lietuvių ir lietuvių-rusų kalbų žodynas'),(7,'','David Christian','Fiction','Dauguma istorikų tyrinėti renkasi labai konkrečias temas: vieną istorinį laikotarpį, iškilų asmenį, reikšmingą įvykį... O kas nutiktų, jei kas nors pamėgintų apžvelgti viską nuo pradžių pradžios, nuo Didžiojo Sprogimo, iki pat šių dienų ir net to, kas pasaulio laukia ateityje? Ar toks plačiai aprėpiantis žvilgsnis pakeistų tai, kaip matome save, kaip suprantame savo vietą Žemėje ir kaip priimame savo egzistenciją šioje begalinėje erdvėje, šiame neaprėpiamame laike?\r\n\r\nTai klausimai, į kuriuos dar 1991-aisiais pasiryžo atsakyti istorikas Davidas Christianas, anksčiau tyrinėjęs nacionalines ir imperijų istorijas, išryškinusias fundamentalų žmonijos susiskaldymą. Mokslininkas norėjo parodyti, jog iš tiesų pasaulis turi vieną istoriją – vieną bendrą pasakojimą apie visa ko kilmę. Taip atsirado Didžiosios istorijos disciplina. Sujungdamas fizikos, biologijos, istorijos ir daugelio kitų mokslo sričių žinias, autorius kuria nuoseklų 13,8 milijardų metų aprėpiantį pasakojimą, gilinasi į kertinius didžiosios istorijos įvykius, nulėmusius svarbiausius pokyčius, ir demonstruoja esmines visa ko sąsajas, pasaulį padariusias tokį, kokį regime šiandien.\r\n\r\n„Kruopščiai supinti faktai ir originalios įžvalgos.\"\r\nBill Gates\r\n\r\n„D. Christianas rado veiksmingą būdą pasinaudoti istorija, kad viskas, ką žinome apie pasaulį, pagaliau rastų sau vietą ir susijungtų į darnų pasakojimą. Stulbinantis pasiekimas.\"\r\nCarlo Rovelli, „Septynių trumpų fizikos pamokų\" autorius\r\n\r\n„Trumpa Visatos istorija. Nuostabiai ir įtaigiai papasakota.\"\r\nWall Street Journal\r\n\r\nDavid Christian (Deividas Kriščianas, g. 1946) – istorijos profesorius, Sidnėjaus Makvorio universiteto Didžiosios istorijos instituto direktorius, daugybės mokslinių straipsnių ir knygų autorius. 2011 m. kartu su Billu Gatesu inicijavo „Didžiosios istorijos projektą\", dėl kurio ši disciplina atsidūrė tūkstančių vidurinių ir aukštųjų mokyklų programose. „Didžioji istorija, arba pasakojimas apie Visatos, Žemės ir žmonijos kilmę\" greitai pelnė skaitytojų simpatijas ir netrukus įsitvirtino tarp perkamiausių „New York Times\" knygų.','hardcover',100,'9786094793332','lithuanian',13.27,416,17.97,'2020-01-22','Baltos lankos',0.2,'DIDŽIOJI ISTORIJA, arba pasakojimas apie Visatos, Žemės ir žmonijos kilmę'),(8,'','Tanya J. Peterson','Management','Ar jaučiatės išsekęs ir prislėgtas dėl nuolat persekiojančio ir verčiančio atsiriboti nerimo? Galbūt siekdamas išsilaisvinti nerimą tik sustiprinate. Nesvarbu, ar įkalintas neramių minčių ir emocijų jaučiatės veik visą gyvenimą, ar tik pastaruoju metu, jūs galite atrasti būdą ištrūkti iš nerimo ir depresijos spąstų.\r\n\r\n„101 būdas suvaldyti nerimą ir depresiją\" – tai jūsų veiksmų planas ir priemonės, kurias pasitelkęs išsilaisvinsite. Šiame asmeninių galių ugdymo vadove rasite:\r\n• 101 pratimą, leisiantį atgauti tokio gyvenimo, kokį norite gyventi, kontrolę;\r\n• penkis atskirus skyrius, kuriuose pristatomos praktinės ir lengvai įvykdomos nerimą malšinančios veiklos;\r\n• būdų, padedančių atsikratyti nuolat kamuojančių įkyrių minčių;\r\n• metodų, leidžiančių susidoroti su nerimu, depresija, vangumu namuose, darbe ar ugdymo įstaigoje;\r\n• priemonių, padedančių įveikti nerimą, kylantį bendraujant su kitais;\r\n• strategijų, kurias pasitelkęs atsikratysite dieną ir naktį kamuojančių rūpesčių;\r\n• veiksmingų praktikų, numalšinančių nerimą.\r\n\r\nLiaukitės kovoti su nerimu ir tiesiog jį paleiskite, imdamasis efektyvių veiksmų. Susikurkite kokybišką gyvenimą be nerimo. Su knyga „101 būdą suvaldyti nerimą ir depresiją\" galite pradėti gyventi nevaržomą, laisvą, visavertį gyvenimą. Atsiverskite knygą ir pradėkite naują gyvenimo skyrių.','hardcover',8,'9786098254372','lithuanian',4.58,255,3.33,'2020-02-04','Liūtai ne avys',0.2,'101 būdas suvaldyti nerimą ir depresiją'),(9,'','Alexander Lowen','Arts','\r\n\r\nDr. Aleksandro Loveno (Alexander Lowen) nuomone, depresijos apimtas žmogus yra praradęs ryšį su realybe, o ypač su savo kūno realybe.\r\n\r\nŠioje įkvepiančioje, novatoriškoje knygoje „Depresija ir kūnas\" tyrinėjamos kultūrinės ir psichologinės priežastys, dėl kurių patenkama į tokią situaciją; joje parodoma, kaip galime įveikti depresiją, atkūrę ryšį su savo fizine savastimi ir mokydamiesi atpažinti savo emocijų išraišką.\r\n\r\nTurėdamas milžinišką darbo su depresija sergančiais pacientais patirtį, autorius pateikia paprastų, bet nepaprastai efektyvių pratimų kompleksą, kurie padės sužadinti įgimtą savo energiją, išreikšti meilę ir savąjį unikalumą, atgaivinti dvasingumą ir tikėjimą gyvenimu.\r\n','hardcover',50,'9786099603353','lithuanian',2.75,288,1.78,'2018-01-09','Liūtai ne avys',0.3,'DEPRESIJA IR KŪNAS'),(10,'','Sarah Wilson','Arts','Kinų išmintis teigia, kad prieš įveikdami pabaisą, pirmiausia turime ją paversti gražia.\r\nŠią kinų patarlę Sara Vilson (Sarah Wilson) pirmiausia aptiko psichiatrės Kėjos Redfild Džeimison (Kay Redfield Jamison) memuaruose „Neramus protas\" (An Unquiet Mind). Ji tapo raktu, padėjusiu rašytojai suprasti visą gyvenimą trunkančią savo pačios kovą su nerimu. S. Vilson – bestselerių autorė, žurnalistė ir verslininkė, kurios mitybos programa ir knyga „Aš atsisakiau cukraus\" (I Quit Sugar) padėjo daugiau nei pusantro milijono žmonių visame pasaulyje gyventi geriau ir sveikiau. Ir visą tą laiką ją lydėjo lėtinis nerimo sutrikimas.\r\n\r\nKnygoje „Pirmiausia paverskime pabaisą gražuole\" S. Vilson pasitelkia savo fantastišką gebėjimą susikoncentruoti, taip pat nuostabius informacijos paieškos įgūdžius, kad geriau suprastų savo viso gyvenimo palydovą ir ištirtų dirgiklius, gydymo būdus, ilgalaikes ir trumpalaikes madas. Autorės literatūros spektras platus, ji kalbina likimo brolius ir seses, psichinės sveikatos ekspertus, filosofus, net patį Dalai Lamą, o viską, ką sužino, perleidžia per savo asmeninių patirčių prizmę.\r\n\r\nS. Vilson skaitytojai atras užuojautą, humorą, bendrystę ir praktinių patarimų, kaip gyventi su savo Pabaisa.\r\n\r\n• Susikurkite dėkingumo ritualą. Negalite vienu metu ir nerimauti, ir būti dėkingas.\r\n• Kad nuslopintumėte nerimą, valgykite. Tikras maistas yra geriausias jūsų draugas.\r\n• Tiesiog kvėpuokite. Atsiverkite gydančiai meditacijos galiai.\r\n• Pasiklokite lovą. Kasdien. Paprasčiausia tvarka išorėje sukuria vidinę ramybę.\r\n• Pasidomėkite kitais nerimautojais, kad pažintumėte save. Emilė Dikinson (Emily Dickinson), Čarlzas Darvinas (Charles Darwin) ir Martinas Liuteris Kingas jaunesnysis (Martin Luther King) irgi kovojo su nerimu.\r\n• Sąmoningai nuspręskite niekur nedalyvauti. Pamirškite baimę, kad praleisite kažką ypatingo, susirangykite ant sofos ir užsisakykite maisto į namus.\r\n\r\nPraktiška, poetiška, išmintinga ir juokinga „Pirmiausia paverskime pabaisą gražuole\" yra maža knyga su didele širdimi. Ji paskatins daugybę nerimo sutrikimą turinčių žmonių priimti šią diagnozę kaip savo esybės dalį ir pažvelgti į jo teikiamas galimybes gyventi turiningesnį, pilnatviškesnį gyvenimą.\r\n\r\nTurbūt geriausia knyga apie gyvenimą su nerimu, kurią kada nors skaičiau, o skaičiau aš jų (deja) daug. Sara siūlo gausybę išmintingų patarimų, tačiau tuo pat metu išlieka labai žmogiška, nekelia savęs į padanges. Pažeidžiamumas yra jos stiprybė. Tikiuosi, perskaitę šią knygą, tą patį apie save galėsite pasakyti ir jūs.\r\n– Mark Manson, knygos „Subtilus menas nekrušti sau (ir kitiems) proto\" autorius\r\n\r\nKandi, kokybiškais šaltiniais paremta ir įžvalgų kupina knyga apie tai, kaip susikurti naujus santykius su nerimu.\r\n– Andrew Solomon, „New York Times\" bestselerio „Far From the Tree\" autorius\r\n\r\nSaros gyvenimo misija – kad kęsdami savo skausmą jaustumėmės nebe tokie vieniši. Šie puslapiai kupini nesumeluotų žodžių ir aiškių nurodymų, kaip grįžti prie savo dvasinės tiesos.\r\n– Gabrielle Bernstein, „New York Times\" bestselerių sąraše pirmąją vietą užėmusios knygos „May Cause Miracles\" autorė\r\n\r\nLabai smagi, išmintinga ir veiksminga knyga apie tai, kaip ištverti nerimą ir susigrąžinti savo gyvenimo kontrolę. Saros kova su nerimu ir jos triumfas įkvepia, o skaitytojas ras naudingų sprendimų. Knygoje „Pirmiausia paverskime pabaisą gražia\" persipina mokslas ir gyvenimiška patirtis. Kitos tokios knygos nežinau.\r\n– Javier Amador, Psichologijos mokslų daktaras, LEAP instituto įkūrėjas\r\n\r\nPaveikūs memuarai apie tai, kaip intensyviai gyvenant susidoroti su nerimu. Nuo nerimo sutrikimų kenčiantiems žmonėms rūpestingai apgalvota ir neretai linksma S. Vilson savianalizė gali tapti pačiu tinkamiausiu kompanionu ir parama.\r\n– Kirkus\r\n\r\nPakylėjanti, rimta praktiška ir kupina nuostabių nukrypimų knyga patiks nerimauti linkusiems žmonėms, kurie šiuose puslapiuose atras daug juos nuraminsiančių dalykų.\r\n– Publishers Weekly\r\n\r\nApie autorę\r\nSara Vilson (Sarah Wilson) – „New York Times\" bestselerių „Pirmiausia paverskime pabaisą gražuole\" ir „Aš atsisakiau cukraus\" (I Quit Sugar) autorė. Ji taip pat sukūrė svetainėje IQuitSugar.com pristatytą aštuonių savaičių trukmės mitybos kursą, kurį 133 šalyse išbandė pusantro milijono žmonių. Buvusi naujienų tarnybos žurnalistė ir „Cosmopolitan\" Australijoje redaktorė dabar rašo apie filosofiją, nerimą, minimalizmą, gyvenimą be toksinų ir antivartotojiškumą – viską galima rasti adresu sarahwilson.com. Autorė gyvena Sidnėjuje, Australijoje, visur važinėja dviračiu, turi priklausomybę nuo žygių ir niekada nestokoja smalsumo.','hardcover',55,'9786099603360','lithuanian',6.89,400,5.55,'2019-12-03','Liūtai ne avys',0.4,'ĮVEIKTAS NERIMAS'),(11,'',' Connirae Andreas, Steve Andreas ','Arts','Neurolingvistinis programavimas, arba NLP, yra mokslas, nagrinėjantis proto veiklą ir pateikiantis apčiuopiamų, kartais – tiesiog stulbinančių rezultatų. NLP siūlo veiksmingas priemones, galinčias padėti išspręsti įvairiausias problemas: atsikratyti žalingų įpročių, numalšinti jaučiamą kaltę, sielvartą, gėdą, scenos baimę ir fobijas, numesti svorio, tinkamai reaguoti į prievartą ir kritiką. NLP taip pat aptaria būdus, kaip padidinti pasitikėjimą savimi, pagerinti santykius su aplinkiniais, tapti labiau nepriklausomu, susikurti teigiamą motyvaciją, numalšinti alergijas maisto produktams.\r\n\r\nDaktarė Connirae Andreas  ir magistras Steve Andreas yra tarptautiniu mastu pripažinti NLP treneriai ir tyrinėtojai, žinomų knygų apie NLP autoriai, žymiausių šios srities veikalų redaktoriai (Richard Bandler „Using Your Brain for a CHANGE“; Richard Bandler, John Grinder, „Frogs into Princes“,„Refr aming: Neuro-Linguistic Programming and the transformation of meaning“ ir kt.).\r\n\r\nSu „NLP Comprehensive“ organizacija jie rengia įvairius seminarus ir ruošia NLP sertifikatus suteikiančias programas, aktyviai dalyvauja kuriant naujus NLP modelius, toliau studijuoja žmonių, turinčių nepaprastų sugebėjimų, mąstymo procesus, kad galėtų sukurti naujų žmogaus sėkmės siekimo modelių.','paperback',100,'9786099547442','lithuanian',2.49,352,1.49,'2020-02-16','Liūtai ne avys',0.3,'Protas turi širdį. Neurolingvistinis programavimas');
/*!40000 ALTER TABLE `book` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `book_book_to_cart_item_list`
--

DROP TABLE IF EXISTS `book_book_to_cart_item_list`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `book_book_to_cart_item_list` (
  `book_id` bigint(20) NOT NULL,
  `book_to_cart_item_list_id` bigint(20) NOT NULL,
  UNIQUE KEY `UK_tc3jnkag591jcmramf4g1xqxg` (`book_to_cart_item_list_id`),
  KEY `FKny36gquom3s3eqvpras5o4i9m` (`book_id`),
  CONSTRAINT `FK4l66ncypltvxegp65jylxwmkl` FOREIGN KEY (`book_to_cart_item_list_id`) REFERENCES `book_to_cart_item` (`id`),
  CONSTRAINT `FKny36gquom3s3eqvpras5o4i9m` FOREIGN KEY (`book_id`) REFERENCES `book` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `book_book_to_cart_item_list`
--

LOCK TABLES `book_book_to_cart_item_list` WRITE;
/*!40000 ALTER TABLE `book_book_to_cart_item_list` DISABLE KEYS */;
/*!40000 ALTER TABLE `book_book_to_cart_item_list` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `book_to_cart_item`
--

DROP TABLE IF EXISTS `book_to_cart_item`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `book_to_cart_item` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `book_id` bigint(20) DEFAULT NULL,
  `cart_item_id` bigint(20) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `FK254kg9aacrs8uqa93ijc3garu` (`book_id`),
  KEY `FKbdyqr108hc7c06xtem0dhv9mk` (`cart_item_id`),
  CONSTRAINT `FK254kg9aacrs8uqa93ijc3garu` FOREIGN KEY (`book_id`) REFERENCES `book` (`id`),
  CONSTRAINT `FKbdyqr108hc7c06xtem0dhv9mk` FOREIGN KEY (`cart_item_id`) REFERENCES `cart_item` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8mb4;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `book_to_cart_item`
--

LOCK TABLES `book_to_cart_item` WRITE;
/*!40000 ALTER TABLE `book_to_cart_item` DISABLE KEYS */;
INSERT INTO `book_to_cart_item` VALUES (1,1,1),(2,2,2),(3,2,3),(4,6,4);
/*!40000 ALTER TABLE `book_to_cart_item` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `cart_item`
--

DROP TABLE IF EXISTS `cart_item`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `cart_item` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `qty` int(11) NOT NULL,
  `subtotal` decimal(19,2) DEFAULT NULL,
  `book_id` bigint(20) DEFAULT NULL,
  `order_id` bigint(20) DEFAULT NULL,
  `shopping_cart_id` bigint(20) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `FKis5hg85qbs5d91etr4mvd4tx6` (`book_id`),
  KEY `FKen9v41ihsnhcr0i7ivsd7i84c` (`order_id`),
  KEY `FKe89gjdx91fxnmkkssyoim8xfu` (`shopping_cart_id`),
  CONSTRAINT `FKe89gjdx91fxnmkkssyoim8xfu` FOREIGN KEY (`shopping_cart_id`) REFERENCES `shopping_cart` (`id`),
  CONSTRAINT `FKen9v41ihsnhcr0i7ivsd7i84c` FOREIGN KEY (`order_id`) REFERENCES `user_order` (`id`),
  CONSTRAINT `FKis5hg85qbs5d91etr4mvd4tx6` FOREIGN KEY (`book_id`) REFERENCES `book` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8mb4;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `cart_item`
--

LOCK TABLES `cart_item` WRITE;
/*!40000 ALTER TABLE `cart_item` DISABLE KEYS */;
INSERT INTO `cart_item` VALUES (1,1,7.64,1,1,NULL),(2,1,90.99,2,2,NULL),(3,2,181.98,2,3,NULL),(4,1,1.23,6,3,NULL);
/*!40000 ALTER TABLE `cart_item` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `hibernate_sequence`
--

DROP TABLE IF EXISTS `hibernate_sequence`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `hibernate_sequence` (
  `next_val` bigint(20) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `hibernate_sequence`
--

LOCK TABLES `hibernate_sequence` WRITE;
/*!40000 ALTER TABLE `hibernate_sequence` DISABLE KEYS */;
INSERT INTO `hibernate_sequence` VALUES (12);
/*!40000 ALTER TABLE `hibernate_sequence` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `password_reset_token`
--

DROP TABLE IF EXISTS `password_reset_token`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `password_reset_token` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `expiry_date` datetime(6) DEFAULT NULL,
  `token` varchar(255) DEFAULT NULL,
  `user_id` bigint(20) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `FK5lwtbncug84d4ero33v3cfxvl` (`user_id`),
  CONSTRAINT `FK5lwtbncug84d4ero33v3cfxvl` FOREIGN KEY (`user_id`) REFERENCES `user` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8mb4;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `password_reset_token`
--

LOCK TABLES `password_reset_token` WRITE;
/*!40000 ALTER TABLE `password_reset_token` DISABLE KEYS */;
INSERT INTO `password_reset_token` VALUES (1,'2021-02-05 19:14:59.000000','8353f85a-f56c-4deb-a8ca-3a9748e10e87',2);
/*!40000 ALTER TABLE `password_reset_token` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `payment`
--

DROP TABLE IF EXISTS `payment`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `payment` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `card_name` varchar(255) DEFAULT NULL,
  `card_number` varchar(255) DEFAULT NULL,
  `cvc` int(11) NOT NULL,
  `expiry_month` int(11) NOT NULL,
  `expiry_year` int(11) NOT NULL,
  `holder_name` varchar(255) DEFAULT NULL,
  `type` varchar(255) DEFAULT NULL,
  `order_id` bigint(20) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `FKt7a73xusjdnnsuespcitb683h` (`order_id`),
  CONSTRAINT `FKt7a73xusjdnnsuespcitb683h` FOREIGN KEY (`order_id`) REFERENCES `user_order` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8mb4;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `payment`
--

LOCK TABLES `payment` WRITE;
/*!40000 ALTER TABLE `payment` DISABLE KEYS */;
INSERT INTO `payment` VALUES (1,NULL,'987654321',154,1,21,'tadijus grutijus','visa',1),(2,NULL,'987654321',154,1,21,'tadijus grutijus','visa',2),(3,NULL,'123456789',323,1,21,'tadijus grutijus','visa',3);
/*!40000 ALTER TABLE `payment` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `role`
--

DROP TABLE IF EXISTS `role`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `role` (
  `role_id` int(11) NOT NULL,
  `name` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`role_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `role`
--

LOCK TABLES `role` WRITE;
/*!40000 ALTER TABLE `role` DISABLE KEYS */;
INSERT INTO `role` VALUES (0,'ROLE_ADMIN'),(1,'ROLE_USER');
/*!40000 ALTER TABLE `role` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `shipping_address`
--

DROP TABLE IF EXISTS `shipping_address`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `shipping_address` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `shipping_address_city` varchar(255) DEFAULT NULL,
  `shipping_address_country` varchar(255) DEFAULT NULL,
  `shipping_address_name` varchar(255) DEFAULT NULL,
  `shipping_address_street1` varchar(255) DEFAULT NULL,
  `shipping_address_street2` varchar(255) DEFAULT NULL,
  `shipping_address_zipcode` varchar(255) DEFAULT NULL,
  `order_id` bigint(20) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `FKatbgaqk1hhhhkyyuebylpeh7q` (`order_id`),
  CONSTRAINT `FKatbgaqk1hhhhkyyuebylpeh7q` FOREIGN KEY (`order_id`) REFERENCES `user_order` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8mb4;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `shipping_address`
--

LOCK TABLES `shipping_address` WRITE;
/*!40000 ALTER TABLE `shipping_address` DISABLE KEYS */;
INSERT INTO `shipping_address` VALUES (1,'mainas','Lithuania','arti','leliju 11','','54789',1),(2,'frankfurt','Lithuania','tadijus','toli 11','','58745',2),(3,'mainas','Lithuania','arti','leliju 11','','54789',3);
/*!40000 ALTER TABLE `shipping_address` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `shopping_cart`
--

DROP TABLE IF EXISTS `shopping_cart`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `shopping_cart` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `grand_total` decimal(19,2) DEFAULT NULL,
  `user_id` bigint(20) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `FK254qp5akhuaaj9n5co4jww3fk` (`user_id`),
  CONSTRAINT `FK254qp5akhuaaj9n5co4jww3fk` FOREIGN KEY (`user_id`) REFERENCES `user` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8mb4;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `shopping_cart`
--

LOCK TABLES `shopping_cart` WRITE;
/*!40000 ALTER TABLE `shopping_cart` DISABLE KEYS */;
INSERT INTO `shopping_cart` VALUES (1,NULL,1),(2,0.00,2);
/*!40000 ALTER TABLE `shopping_cart` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `user`
--

DROP TABLE IF EXISTS `user`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `user` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `email` varchar(255) NOT NULL,
  `enabled` bit(1) NOT NULL,
  `first_name` varchar(255) DEFAULT NULL,
  `last_name` varchar(255) DEFAULT NULL,
  `password` varchar(255) DEFAULT NULL,
  `phone` varchar(255) DEFAULT NULL,
  `username` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8mb4;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `user`
--

LOCK TABLES `user` WRITE;
/*!40000 ALTER TABLE `user` DISABLE KEYS */;
INSERT INTO `user` VALUES (1,'admin@jh.lt','','','','$2a$12$i67EhfS3Hs00vQMOX2/ys./N6fgdJbSlyu/wfdmWSjWcswKiUEq7m',NULL,'admin'),(2,'grusinskis@gmail.com','','tadijus','grutijus','$2a$12$3LHjo.vLzT52f1cZ5YEopuAY1wzbe0udYeNkyc/PyoFM0pwy9rNqK',NULL,'grusius');
/*!40000 ALTER TABLE `user` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `user_billing`
--

DROP TABLE IF EXISTS `user_billing`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `user_billing` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `user_billing_city` varchar(255) DEFAULT NULL,
  `user_billing_country` varchar(255) DEFAULT NULL,
  `user_billing_name` varchar(255) DEFAULT NULL,
  `user_billing_street1` varchar(255) DEFAULT NULL,
  `user_billing_street2` varchar(255) DEFAULT NULL,
  `user_billing_zipcode` varchar(255) DEFAULT NULL,
  `user_payment_id` bigint(20) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `FK3v6hd7snyc3g9s72u41k1fydu` (`user_payment_id`),
  CONSTRAINT `FK3v6hd7snyc3g9s72u41k1fydu` FOREIGN KEY (`user_payment_id`) REFERENCES `user_payment` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8mb4;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `user_billing`
--

LOCK TABLES `user_billing` WRITE;
/*!40000 ALTER TABLE `user_billing` DISABLE KEYS */;
INSERT INTO `user_billing` VALUES (1,'klaipeda','Lithuania','namai1','kazkur 1','kazkur 2','93256',1),(2,'kaunas','Lithuania','namai2','belenkur 1','','14265',2);
/*!40000 ALTER TABLE `user_billing` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `user_order`
--

DROP TABLE IF EXISTS `user_order`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `user_order` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `shipping_method` varchar(255) DEFAULT NULL,
  `order_date` datetime(6) DEFAULT NULL,
  `order_status` varchar(255) DEFAULT NULL,
  `order_total` decimal(19,2) DEFAULT NULL,
  `shipping_date` datetime(6) DEFAULT NULL,
  `billing_address_id` bigint(20) DEFAULT NULL,
  `payment_id` bigint(20) DEFAULT NULL,
  `shipping_address_id` bigint(20) DEFAULT NULL,
  `user_id` bigint(20) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `FKbaytj4l2p74kc5dp2dcrhucjo` (`billing_address_id`),
  KEY `FKqjg5jrh5qwnhl2f9lk7n77454` (`payment_id`),
  KEY `FKo2lj94xaujs1se8whlhc37nj7` (`shipping_address_id`),
  KEY `FKj86u1x7csa8yd68ql2y1ibrou` (`user_id`),
  CONSTRAINT `FKbaytj4l2p74kc5dp2dcrhucjo` FOREIGN KEY (`billing_address_id`) REFERENCES `billing_address` (`id`),
  CONSTRAINT `FKj86u1x7csa8yd68ql2y1ibrou` FOREIGN KEY (`user_id`) REFERENCES `user` (`id`),
  CONSTRAINT `FKo2lj94xaujs1se8whlhc37nj7` FOREIGN KEY (`shipping_address_id`) REFERENCES `shipping_address` (`id`),
  CONSTRAINT `FKqjg5jrh5qwnhl2f9lk7n77454` FOREIGN KEY (`payment_id`) REFERENCES `payment` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8mb4;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `user_order`
--

LOCK TABLES `user_order` WRITE;
/*!40000 ALTER TABLE `user_order` DISABLE KEYS */;
INSERT INTO `user_order` VALUES (1,'groundShipping','2021-02-05 17:57:08.000000','Delivered',7.64,NULL,1,1,1,2),(2,'groundShipping','2021-02-05 17:57:45.000000','created',90.99,NULL,2,2,2,2),(3,'groundShipping','2021-02-06 15:25:59.000000','created',183.21,NULL,3,3,3,2);
/*!40000 ALTER TABLE `user_order` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `user_payment`
--

DROP TABLE IF EXISTS `user_payment`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `user_payment` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `card_name` varchar(255) DEFAULT NULL,
  `card_number` varchar(255) DEFAULT NULL,
  `cvc` int(11) NOT NULL,
  `default_payment` bit(1) NOT NULL,
  `expiry_month` int(11) NOT NULL,
  `expiry_year` int(11) NOT NULL,
  `holder_name` varchar(255) DEFAULT NULL,
  `type` varchar(255) DEFAULT NULL,
  `user_id` bigint(20) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `FK8fb9fr82lb1qk2cw55ito9rk6` (`user_id`),
  CONSTRAINT `FK8fb9fr82lb1qk2cw55ito9rk6` FOREIGN KEY (`user_id`) REFERENCES `user` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8mb4;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `user_payment`
--

LOCK TABLES `user_payment` WRITE;
/*!40000 ALTER TABLE `user_payment` DISABLE KEYS */;
INSERT INTO `user_payment` VALUES (1,'pirma','123456789',323,'',9,2021,'tadijus grutijus','visa',2),(2,'antra','987654321',154,'\0',1,2021,'tadijus grutijus','mastercard',2);
/*!40000 ALTER TABLE `user_payment` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `user_role`
--

DROP TABLE IF EXISTS `user_role`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `user_role` (
  `user_role_id` bigint(20) NOT NULL AUTO_INCREMENT,
  `role_id` int(11) DEFAULT NULL,
  `user_id` bigint(20) DEFAULT NULL,
  PRIMARY KEY (`user_role_id`),
  KEY `FKa68196081fvovjhkek5m97n3y` (`role_id`),
  KEY `FK859n2jvi8ivhui0rl0esws6o` (`user_id`),
  CONSTRAINT `FK859n2jvi8ivhui0rl0esws6o` FOREIGN KEY (`user_id`) REFERENCES `user` (`id`),
  CONSTRAINT `FKa68196081fvovjhkek5m97n3y` FOREIGN KEY (`role_id`) REFERENCES `role` (`role_id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8mb4;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `user_role`
--

LOCK TABLES `user_role` WRITE;
/*!40000 ALTER TABLE `user_role` DISABLE KEYS */;
INSERT INTO `user_role` VALUES (1,0,1),(2,1,2);
/*!40000 ALTER TABLE `user_role` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `user_shipping`
--

DROP TABLE IF EXISTS `user_shipping`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `user_shipping` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `user_shipping_city` varchar(255) DEFAULT NULL,
  `user_shipping_country` varchar(255) DEFAULT NULL,
  `user_shipping_default` bit(1) NOT NULL,
  `user_shipping_name` varchar(255) DEFAULT NULL,
  `user_shipping_street1` varchar(255) DEFAULT NULL,
  `user_shipping_street2` varchar(255) DEFAULT NULL,
  `user_shipping_zipcode` varchar(255) DEFAULT NULL,
  `user_id` bigint(20) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `FK9hidca5hndj9y0b5jb0xtpn9u` (`user_id`),
  CONSTRAINT `FK9hidca5hndj9y0b5jb0xtpn9u` FOREIGN KEY (`user_id`) REFERENCES `user` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8mb4;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `user_shipping`
--

LOCK TABLES `user_shipping` WRITE;
/*!40000 ALTER TABLE `user_shipping` DISABLE KEYS */;
INSERT INTO `user_shipping` VALUES (1,'frankfurt','Germany','','tadijus','toli 11','','58745',2),(2,'mainas','Germany','','arti','leliju 11','','54789',2);
/*!40000 ALTER TABLE `user_shipping` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Dumping routines for database 'bookstoredatabase'
--
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2021-02-07  9:48:26
