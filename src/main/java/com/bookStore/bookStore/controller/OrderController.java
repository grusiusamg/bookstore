package com.bookStore.bookStore.controller;

import com.bookStore.bookStore.domain.CartItem;
import com.bookStore.bookStore.domain.Order;
import com.bookStore.bookStore.services.OrderService;
import com.bookStore.bookStore.services.CartItemService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

import java.util.Arrays;
import java.util.List;

@Controller
public class OrderController {

    @Autowired
    private OrderService orderService;

    @Autowired
    private CartItemService cartItemService;


    @RequestMapping("/orderList")
    public String orderList(Model model) {
        List<Order> orderList = orderService.findAll();
        model.addAttribute("orderList", orderList);

        return "orderList";
    }

    @RequestMapping("/orderDetails")
    public String orderDetails(
            @RequestParam("id") Long orderId, Model model
    ) {
        Order order = orderService.findById(orderId).get();
        model.addAttribute("order", order);

        List<CartItem> cartItemList = cartItemService.findByOrder(order);
        model.addAttribute("cartItemList", cartItemList);

        List<String> selectList = Arrays.asList("Confirmed","Packing","Send","Delivered","Canceled");

        model.addAttribute("selectList", selectList);
        model.addAttribute("select", "Confirmed");

        return "orderDetails";
    }

    @RequestMapping("/updateOrderStatus")
    public String updateOrderStatus(
            @ModelAttribute("id") Long orderId,
            @ModelAttribute("select") String select
    ) {
        Order order = orderService.findById(orderId).get();
        order.setOrderStatus(select);
        orderService.updateOrder(order);

       return "forward:/orderDetails";
    }

}
