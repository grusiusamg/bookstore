package com.bookStore.bookStore.controller;

import com.bookStore.bookStore.domain.Book;
import com.bookStore.bookStore.domain.CartItem;
import com.bookStore.bookStore.domain.ShoppingCart;
import com.bookStore.bookStore.domain.User;
import com.bookStore.bookStore.services.BookService;
import com.bookStore.bookStore.services.CartItemService;
import com.bookStore.bookStore.services.ShoppingCartService;
import com.bookStore.bookStore.services.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

import java.security.Principal;
import java.util.List;

@Controller
@RequestMapping("/")
public class ShoppingCartController {

    @Autowired
    private UserService userService;

    @Autowired
    private CartItemService cartItemService;

    @Autowired
    private ShoppingCartService shoppingCartService;

    @Autowired
    private BookService bookService;

    @RequestMapping("/cart")
    public String shoppingCart(Model model, Principal principal) {
        User user = userService.findByUsername(principal.getName());
        ShoppingCart shoppingCart = user.getShoppingCart();

        List<CartItem> cartItemList = cartItemService.findByShoppingCart(shoppingCart);

        shoppingCartService.updateShoppingCart(shoppingCart);

        model.addAttribute("cartItemList", cartItemList);
        model.addAttribute("shoppingCart", shoppingCart);

        return "shoppingCart";
    }

    @RequestMapping("/addItem")
    public String addItem(
            @ModelAttribute("book") Book book,
            @ModelAttribute("qty") String qty,
            Model model, Principal principal
    ) {
        User user = userService.findByUsername(principal.getName());
        book = bookService.findById(book.getId()).get();

        if (Integer.parseInt(qty) > book.getInStockNumber()) {
            model.addAttribute("notEnoughStock", true);
            return "forward:/bookDetail?id=" + book.getId();
        }
        CartItem cartItem = cartItemService.addBookToCartItem(book, user, Integer.parseInt(qty));
        model.addAttribute("addBookSuccess", true);

        return "forward:/bookDetail?id=" + book.getId();
    }

    @RequestMapping("/updateCartItem")
    public String updateShoppingCartItem(
            @ModelAttribute("id") Long cartItemId,
            @ModelAttribute("qty") int qty
    ) {
        CartItem cartItem1 = cartItemService.findById(cartItemId).get();
        cartItem1.setQty(qty);
        cartItemService.updateCartItem(cartItem1);

        return "forward:/cart";
    }

    @RequestMapping("/removeItem")
    public String removeItem(
            @RequestParam("id") Long id) {
        cartItemService.removeCartItem(cartItemService.findById(id).get());

        return "forward:/cart";
    }
}
