package com.bookStore.bookStore.model;

public enum EuNames {
    Lt("Lithuania"),
    LV("Latvia"),
    PL("Poland"),
    AU("Austria"),
    BE("Belgium"),
    BU("Bulgaria"),
    CR("Croatia"),
    CY("Cyprus"),
    CZ("Czechia"),
    DE("Denmark"),
    ES("Estonia"),
    FI("Finland"),
    FR("France"),
    GE("Germany"),
    GR("Greece"),
    HR("Hungary"),
    IR("Ireland"),
    IT("Italy"),
    LU("Luxembourg"),
    MA("Malta"),
    NE("Netherlands"),
    PO("Portugal"),
    RO("Romania"),
    SL("Slovakia"),
    SV("Slovenia"),
    SP("Spain"),
    SW("Sweden");

    private final String displayValue;

    private EuNames(String displayValue) {
        this.displayValue = displayValue;
    }

    public String getDisplayValue() {
        return displayValue;
    }
}
