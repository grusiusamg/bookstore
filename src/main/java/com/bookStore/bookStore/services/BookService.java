package com.bookStore.bookStore.services;

import com.bookStore.bookStore.domain.Book;

import java.util.List;
import java.util.Optional;

public interface BookService {
    List<Book> findAll();

    Optional<Book> findById(Long id);

    List<Book> findByCategory(String category);

    List<Book> blurrySearch(String title);

    Book save(Book book);

    void removeById(Long id);
}
