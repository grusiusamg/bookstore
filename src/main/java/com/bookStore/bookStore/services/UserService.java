package com.bookStore.bookStore.services;

import com.bookStore.bookStore.domain.User;
import com.bookStore.bookStore.domain.UserBilling;
import com.bookStore.bookStore.domain.UserPayment;
import com.bookStore.bookStore.domain.UserShipping;
import com.bookStore.bookStore.domain.security.PasswordResetToken;
import com.bookStore.bookStore.domain.security.UserRole;

import java.util.Optional;
import java.util.Set;

public interface UserService {
    PasswordResetToken getPasswordResetToken(final String token);

    void createPasswordResetTokenForUser(final User user, final String token);

    User findByUsername(String username);

    User findByEmail(String email);

    Optional<User> findById(Long id);

    User createUser(User user, Set<UserRole> userRoles) throws Exception;

    User save(User user);

    void updateUserBilling(UserBilling userBilling, UserPayment userPayment, User user);

    void updateUserShipping(UserShipping userShipping, User user);

    void setUserDefaultPayment(Long userPaymentId, User user);

    void setUserDefaultShipping(Long userShippingId, User user);

}
