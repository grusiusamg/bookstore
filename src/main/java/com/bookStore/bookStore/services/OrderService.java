package com.bookStore.bookStore.services;

import com.bookStore.bookStore.domain.*;

import java.util.List;
import java.util.Optional;

public interface OrderService {
    Order createOrder(ShoppingCart shoppingCart, ShippingAddress shippingAddress,
                      BillingAddress billingAddress, Payment payment, String shippingMethod, User user);

    Optional<Order> findById(Long id);

    Order updateOrder(Order order);

    List<Order> findAll();
}
