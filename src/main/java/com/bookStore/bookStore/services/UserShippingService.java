package com.bookStore.bookStore.services;

import com.bookStore.bookStore.domain.UserShipping;

import java.util.Optional;

public interface UserShippingService {

    Optional<UserShipping> findById(Long id);

    void removeById(Long id);
}
