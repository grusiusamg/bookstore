package com.bookStore.bookStore.services;

import com.bookStore.bookStore.domain.ShippingAddress;
import com.bookStore.bookStore.domain.UserShipping;

public interface ShippingAddressService {
    ShippingAddress setByUserShipping(UserShipping userShipping,ShippingAddress shippingAddress);
}
