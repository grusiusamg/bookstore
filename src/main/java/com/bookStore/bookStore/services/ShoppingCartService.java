package com.bookStore.bookStore.services;

import com.bookStore.bookStore.domain.ShoppingCart;


public interface ShoppingCartService {
    ShoppingCart updateShoppingCart(ShoppingCart shoppingCart);

    void clearShoppingCart(ShoppingCart shoppingCart);
}

