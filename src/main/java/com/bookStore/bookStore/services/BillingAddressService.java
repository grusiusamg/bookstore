package com.bookStore.bookStore.services;

import com.bookStore.bookStore.domain.BillingAddress;
import com.bookStore.bookStore.domain.UserBilling;

public interface BillingAddressService {
    BillingAddress setByUserBilling(UserBilling userBilling, BillingAddress billingAddress);
}
