package com.bookStore.bookStore.services.ipml;

import com.bookStore.bookStore.domain.UserPayment;
import com.bookStore.bookStore.repository.UserPaymentRepository;
import com.bookStore.bookStore.services.UserPaymentService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Optional;

@Service
public class UserPaymentServiceImpl implements UserPaymentService {

    @Autowired
    private UserPaymentRepository userPaymentRepository;

    public Optional<UserPayment> findById(Long id){
        return userPaymentRepository.findById(id);
    }

    public void removeById(Long id) {
        userPaymentRepository.deleteById(id);
    }
}
