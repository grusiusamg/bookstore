package com.bookStore.bookStore.services.ipml;

import com.bookStore.bookStore.domain.UserShipping;
import com.bookStore.bookStore.repository.UserShippingRepository;
import com.bookStore.bookStore.services.UserShippingService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Optional;

@Service
public class UserShippingServiceImpl implements UserShippingService {

    @Autowired
    private UserShippingRepository userShippingRepository;

   public Optional<UserShipping> findById(Long id){
       return userShippingRepository.findById(id);
   }

   public void removeById(Long id){
       userShippingRepository.deleteById(id);
   }
}
