package com.bookStore.bookStore.services.ipml;

import com.bookStore.bookStore.domain.BillingAddress;
import com.bookStore.bookStore.domain.UserBilling;
import com.bookStore.bookStore.services.BillingAddressService;
import org.springframework.stereotype.Service;

@Service
public class BillingAddressServiceImpl implements BillingAddressService {

    public BillingAddress setByUserBilling(UserBilling userBilling, BillingAddress billingAddress) {
        userBilling.setUserBillingName(userBilling.getUserBillingName());
        userBilling.setUserBillingStreet1(userBilling.getUserBillingStreet1());
        userBilling.setUserBillingStreet2(userBilling.getUserBillingStreet2());
        userBilling.setUserBillingCity(userBilling.getUserBillingCity());
        userBilling.setUserBillingCountry(userBilling.getUserBillingCountry());
        userBilling.setUserBillingZipcode(userBilling.getUserBillingZipcode());

        return billingAddress;
    }
}


