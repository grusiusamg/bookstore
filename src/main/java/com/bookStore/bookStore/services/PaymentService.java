package com.bookStore.bookStore.services;

import com.bookStore.bookStore.domain.Payment;
import com.bookStore.bookStore.domain.UserPayment;

public interface PaymentService {

    Payment setByUserPayment(UserPayment userPayment, Payment payment);
}
