package com.bookStore.bookStore.repository;

import com.bookStore.bookStore.domain.Order;
import org.springframework.data.repository.CrudRepository;

public interface OrderRepository extends CrudRepository<Order, Long> {
}
