package com.bookStore.bookStore.repository;

import com.bookStore.bookStore.domain.CartItem;
import com.bookStore.bookStore.domain.Order;
import com.bookStore.bookStore.domain.ShoppingCart;
import org.springframework.data.repository.CrudRepository;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;


@Transactional
public interface CartItemRepository extends CrudRepository<CartItem, Long> {
    List<CartItem> findByShoppingCart(ShoppingCart shoppingCart);

    List<CartItem> findByOrder(Order order);
}
