package com.bookStore.bookStore.repository;

import com.bookStore.bookStore.domain.UserPayment;
import org.springframework.data.repository.CrudRepository;

public interface UserPaymentRepository extends CrudRepository<UserPayment, Long>{

}
