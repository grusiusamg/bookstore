package com.bookStore.bookStore.repository;

import com.bookStore.bookStore.domain.BookToCartItem;
import com.bookStore.bookStore.domain.CartItem;
import org.springframework.data.repository.CrudRepository;
import org.springframework.transaction.annotation.Transactional;

@Transactional
public interface BookToCartItemRepository extends CrudRepository<BookToCartItem, Long> {

    void deleteByCartItem(CartItem cartItem);
}
