package com.bookStore.bookStore.repository;


import com.bookStore.bookStore.domain.ShoppingCart;
import org.springframework.data.repository.CrudRepository;


public interface ShoppingCartRepository extends CrudRepository<ShoppingCart, Long> {

}
