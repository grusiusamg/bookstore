package com.bookStore.bookStore.repository;

import com.bookStore.bookStore.domain.UserShipping;
import org.springframework.data.repository.CrudRepository;

public interface UserShippingRepository extends CrudRepository<UserShipping, Long > {


}
