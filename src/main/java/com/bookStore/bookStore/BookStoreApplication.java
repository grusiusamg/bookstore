package com.bookStore.bookStore;

import com.bookStore.bookStore.domain.User;
import com.bookStore.bookStore.domain.security.Role;
import com.bookStore.bookStore.domain.security.UserRole;
import com.bookStore.bookStore.services.UserService;
import com.bookStore.bookStore.utility.SecurityUtility;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.builder.SpringApplicationBuilder;
import org.springframework.boot.web.servlet.support.SpringBootServletInitializer;

import java.util.HashSet;
import java.util.Set;

@SpringBootApplication
public class BookStoreApplication extends SpringBootServletInitializer implements CommandLineRunner {

    @Autowired
    private UserService userService;

    public static void main(String[] args) {
        SpringApplication.run(BookStoreApplication.class, args);
    }

    @Override
    protected SpringApplicationBuilder configure(SpringApplicationBuilder builder) {
        return builder.sources(BookStoreApplication.class);
    }

    @Override
    public void run(String... args) throws Exception {
        User user1 = new User();
        user1.setFirstName("");
        user1.setLastName("");
        user1.setUsername("admin");
        user1.setPassword(SecurityUtility.passwordEncoder().encode("123456"));
        user1.setEmail("admin@jh.lt");
        Set<UserRole> userRoles = new HashSet<>();
        Role role1 = new Role();
        role1.setRoleId(0);
        role1.setName("ROLE_ADMIN");
        userRoles.add(new UserRole(user1, role1));

        userService.createUser(user1, userRoles);

    }

}
